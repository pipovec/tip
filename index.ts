import { MainProgram } from "./MainProgram";

export function main()
{
  const mainProgram = new MainProgram();
  mainProgram.run();
}

main();

