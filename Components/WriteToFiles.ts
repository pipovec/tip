class WriteToFiles
{
    private fs = require('fs');

    public appendDataToFile(filename: string, data: string)
    {
        this.fs.appendFileSync(filename, data+"\n");
    }


}

export {WriteToFiles}