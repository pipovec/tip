
import {Interface} from './Interface/componentInterface';
import { IDataType } from './Interface/dataTypeInterface';

 class Humidity implements Interface
{
    private responseData: IDataType;

    private max: number = 40;
    private readonly min: number = 60;
    private readonly guid: string = 'b992d997-74ba-4a9f-b2f3-7305f135cf81';
    private readonly sensor: string = 'humidity'


    constructor () {
        this.responseData = {
            'guid': this.guid,
            'sensor' : this.sensor,
            'value': this.generateValue(),
            'date': this.generateDate(),
        }
    }

    generetaData(): string {
        return JSON.stringify(this.responseData);
    }

    generateValue(): number
    {
        const value = Math.floor(Math.random() * this.max - this.min + this.min);
        return value;
    }

    generateDate(): string
    {
        const date = new Date().toISOString();
        return date;
    }

}
export { Humidity };
