interface DataTypeInterface
{
    guid: string,
    sensor: string,
    value: number,
    date: string
}
export { DataTypeInterface as IDataType }