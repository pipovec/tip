import {Interface} from './Interface/componentInterface';
import { IDataType } from './Interface/dataTypeInterface';

class Luminous implements Interface
{
    responseData: IDataType;

    private max: number = 700;
    private min: number = 0;

    constructor () {
        this.responseData = {
            'guid': '6060356b-8fba-410c-b7f2-ed906f30105d',
            'sensor' : 'luminous',
            'value': this.generateValue(),
            'date': this.generateDate()
        }
    }

    generetaData(): string {
        return JSON.stringify(this.responseData);
    }

    generateValue(): number {
        const value = Math.floor(Math.random() * this.max - this.min + this.min);
        return value;
    }

    generateDate(): string {
        const date = new Date().toISOString();
        return date;
    }

}

export { Luminous }
