import { Interface } from "./Interface/componentInterface";
import { IDataType } from './Interface/dataTypeInterface';

class Temperature implements Interface
{
    private responseData: IDataType;

    private max: number = 24;
    private min: number = 18;

    constructor () {
        this.responseData = {
            'guid': '10158861-d430-4edf-bb88-fac67e515221',
            'sensor' : 'temperature',
            'value': this.generateValue(),
            'date': this.generateDate(),
        }
    }

    generetaData(): string {
        return JSON.stringify(this.responseData);
    }
    generateValue(): number {
        const value = Math.floor(Math.random() * this.max - this.min + this.min);
        return value;
    }
    generateDate(): string {
        const date = new Date().toISOString();
        return date;
    }

}

export {Temperature}