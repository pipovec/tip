import { Humidity } from "./humidity"
import { Luminous } from "./luminous";
import { Temperature } from "./temperature";

class DataGenerator
{

    getHumidity(): string
    {
        const humidityClass = new Humidity();
        return humidityClass.generetaData();
    }

    getLuminous(): string
    {
        const luminousClass = new Luminous();
        return luminousClass.generetaData();
    }

    getTemperature(): string
    {
        const temperatureClass = new Temperature();
        return temperatureClass.generetaData();
    }
}
export {DataGenerator}