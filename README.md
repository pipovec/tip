tip
1. Po naklonovani git repositara treba spustit prikaz `npm i` ktore nainstaluje node moduly do adresara node_modules


2. prikaz `tsc` skompiluje typescript do adresara `dist`


3. Spustenie programu sa zatial spusti prikazom `node ./dist/index.js` alebo aj `npm start`


4. Je potrebne mat nainstalovane mosquito v Ubuntu. 


5. Server ktory bude pocuvat `mosquitto_sub -t "pevsTopic" -u "deamon" -P "sedemtri"` meno a heslo je pre moj pocitac.


6. Jednotlive senzory su rozdelene do viacerych topicov napr. `mosquitto_sub -t "pevsTopic/humidity" -u "deamon" -P "sedemtri"` meno a heslo je pre moj pocitac.


7. Zmena intervalu `mosquitto_pub -t "pevsTopic/settings/interval" -u "deamon" -P "sedemtri" -m '{"timeInterval": 5000}'` 


8. Vypnutie alebo zapnutie funkcie `mosquitto_pub -t "pevsTopic/settings/function" -u "deamon" -P "sedemtri" -m '{"functionName": "humidity", "enable": false}'`


9. Vypnutie programu `mosquitto_pub -t "pevsTopic/settings/function" -u "deamon" -P "sedemtri" -m '{"functionName": "shutdown"}'`

