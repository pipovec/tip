import { DataGenerator } from "./Components/DataGenerator"
import { MqttClients } from "./Components/MqttClient";
import { WriteToFiles } from "./Components/WriteToFiles";

class MainProgram
{
    private dataGenerator: DataGenerator;
    private mqttClient: any;
    interval: number = 500;
    humidity: boolean = true;
    luminous: boolean = true;
    temperature: boolean = true;
    logger: boolean = false;
    refreshInterval: any;

    constructor()
    {
        this.dataGenerator = new DataGenerator();
        this.mqttClient = MqttClients.getBroker();

        this.mqttClient.on('connect', () => {
            this.mqttClient.subscribe('pevsTopic/settings/interval');
            this.mqttClient.subscribe('pevsTopic/settings/function');
        });
        this.waitForMessage();
    }

    private waitForMessage()
    {
        this.mqttClient.on('message', (topic: string, message: string)=>{
            switch(topic){
                case 'pevsTopic/settings/interval':
                    this.changeInterval(message);

                case 'pevsTopic/settings/function':
                    this.changeFunction(message);
            }

        });
    }

    private changeInterval(message: string): void
    {
        const json = JSON.parse(message.toString());

        this.interval = Number(json.timeInterval);

        clearInterval(this.refreshInterval);
        this.run();
    }

    private changeFunction(message: string): void
    {
        const json = JSON.parse(message.toString());

        if (json.functionName === 'humidity')
        {
            this.humidity = json.enable;
        }

        if (json.functionName === 'luminous')
        {
            this.luminous = json.enable;
        }

        if (json.functionName === 'temeprature')
        {
            this.temperature = json.enable;
        }

        if (json.functionName === 'logger')
        {
            this.logger = json.enable;
        }

        if (json.functionName === 'shutdown')
        {
            this.terminateProgram();
        }
    }


    private terminateProgram()
    {
        console.log('This process is your pid ' + process.pid);
        clearInterval(this.refreshInterval);
        this.humidity = false;
        this.luminous = false;
        this.temperature = false;


        process.exit(0)
    }


    private publishJson()
    {
        const writeLogData = new WriteToFiles();

        if(this.humidity)
        {
            const dataHumidity = this.dataGenerator.getHumidity();

            writeLogData.appendDataToFile('./log/humidities.log', dataHumidity);

            if(this.logger === true)
            {
                console.log(dataHumidity);
            }

            this.mqttClient.publish('pevsTopic/humidity', dataHumidity);
        }

        if(this.luminous)
        {
            const dataLuminous = this.dataGenerator.getLuminous();

            writeLogData.appendDataToFile('./log/luminous.log', dataLuminous);

            if(this.logger === true)
            {
                console.log(dataLuminous);
            }

            this.mqttClient.publish('pevsTopic/luminous', dataLuminous);
        }

        if(this.temperature)
        {
            const dataTemperature = this.dataGenerator.getTemperature();

            writeLogData.appendDataToFile('./log/temperatures.log', dataTemperature);

            if(this.logger === true)
            {
                console.log(dataTemperature);
            }

            this.mqttClient.publish('pevsTopic/temperature', dataTemperature);
        }
    }

    public run()
    {
        this.refreshInterval = setInterval(this.publishJson.bind(this), this.interval);
    }

}

export {MainProgram}